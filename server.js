const express = require('express');
const server = express();

server.use(express.urlencoded({extended:true}));
server.use(express.json());

//midleware global, genero un log con cada ejecucion

server.use(function(req,res,next){
    console.log(req.url)
    next()
});

const users = [
    {id:1,nombre:"Pipe",email:"Pipe@nada.com"},
    {id:2,nombre:"Daniel",email:"daniel@nada.com"},
    {id:3,nombre:"Kevin",email:"kevin@nada.com"},
    {id:4,nombre:"Maria",email:"maria@nada.com"},
];

server.get('/users', function(req,res){
    res.json(users)
});

server.get('/users/:id', function (req, res) {
    let user_id = parseInt(req.params.id)
    let indice = false
    let mi_respuesta = {user_id: user_id};

    const result = users.filter(user => user.id == user_id);
    console.log(result[0])
    if(result[0] === undefined){
        mi_respuesta.msj = "Usuario no encontrado"
    }else{
        mi_respuesta.msj = "Usuario encontrado"
        mi_respuesta.user = result[0]
    }

    res.json(mi_respuesta)
    
});


server.post('/users_by_name', function (req, res) {

    let name = req.body.name
    let mi_respuesta = {search: name}

    const result = users.filter(user => user.nombre.toLowerCase() === name.toLowerCase());


    if(result.length == 0){
        mi_respuesta.msj = "Usuario no encontrado"
    }else{
        mi_respuesta.msj = "Usuario encontrado"
        mi_respuesta.users = result
    }

    res.json(mi_respuesta)
    
});



server.listen(3000,function(){
    console.log('Corriendo en http://localhost:3000');
 });



